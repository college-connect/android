package com.fardo.app.fardo.services;

import android.app.IntentService;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.data.FeedContract;

import com.fardo.app.fardo.data.FeedContract.LabelEntry;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

/**
 * Created by thespidy on 07/06/16.
 */
public class PrefetchIntentService extends IntentService{
    private static final String TAG = "PrefetchService";

    public PrefetchIntentService() {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        fetchData();
    }

    private void fetchData() {
        try {
            FardoApplication appInstance = FardoApplication.getInstance();
            final String url = Constants.FARDO_BASE_URL + "groups/" + appInstance.getGroupId() + "/labels";

            JSONObject params = new JSONObject();

            JsonRequest request = appInstance.getRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        extractLabelsData(response.getJSONArray("data"));
                    } catch (JSONException ex) {

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                }
            }, url);

            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }

    private void extractLabelsData(JSONArray labelsArray) throws JSONException {
        Vector<ContentValues> labelsContentVector = new Vector<ContentValues>(labelsArray.length());
        for (int i = 0; i < labelsArray.length(); i++) {

            ContentValues myCircleValues = new ContentValues();
            JSONObject label = labelsArray.getJSONObject(i);

            myCircleValues.put(LabelEntry.COLUMN_LABEL_ID, label.getString("id"));
            myCircleValues.put(LabelEntry.COLUMN_NAME, label.getString("name"));
            myCircleValues.put(LabelEntry.COLUMN_DESCRIPTION, label.getString("description"));
            labelsContentVector.add(myCircleValues);
        }

        if (labelsContentVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[labelsContentVector.size()];
            labelsContentVector.toArray(cvArray);

            getContentResolver().delete(LabelEntry.CONTENT_URI, null, null);
            getContentResolver().bulkInsert(LabelEntry.CONTENT_URI, cvArray);
        }
    }
}

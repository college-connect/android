package com.fardo.app.fardo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.fardo.app.fardo.fragments.CreatePostFragment;
import com.fardo.app.fardo.fragments.PostDetailFragment;
import com.fardo.app.fardo.models.Post;

/**
 * Created by thespidy on 17/05/16.
 */
public class PostDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: Change the layout settings
        setContentView(R.layout.activity_post_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Post post = (Post)bundle.getSerializable("post");
        Bundle args = new Bundle();
        args.putSerializable("post", post);
        Fragment fragment = PostDetailFragment.newInstance(1,"post details");
        fragment.setArguments(args);
        ft.add(R.id.postDetails,fragment);
        ft.commit();
    }
}

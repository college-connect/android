package com.fardo.app.fardo.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fardo.app.fardo.PostDetailActivity;
import com.fardo.app.fardo.R;
import com.fardo.app.fardo.adapters.PostsAdapter;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.data.FeedContract.FeedEntry;
import com.fardo.app.fardo.models.Post;
import com.fardo.app.fardo.sync.FardoSyncAdapter;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Handler;

/**
 * Created by thespidy on 24/06/16.
 */
public class FeedFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = MyCircleFragment.class.getSimpleName();
    private Activity mThisActivity;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private PostsAdapter mAdapter;
    private ArrayList<Post> mPosts = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private BottomSheetLayout mBottomSheet;

    // newInstance constructor for creating fragment with arguments
    public static MyCircleFragment newInstance(int page, String title) {
        MyCircleFragment fragmentFirst = new MyCircleFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThisActivity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_circle, container, false);
        bindViewVariables(view);
        return view;
    }

    private void bindViewVariables(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_circle_recycler);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mThisActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PostsAdapter(mThisActivity, mPosts);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_my_circle);
        mBottomSheet = (BottomSheetLayout) view.findViewById(R.id.bottomsheet);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(LOG_TAG, "Swipe refresh");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mAdapter.setPostViewClickListener(new PostsAdapter.IPostViewHolderClick() {
            @Override
            public void onUpvote(Post p) {
                Log.d(LOG_TAG, p.toString());
            }

            @Override
            public void onDownvote(Post p) {
                Log.d(LOG_TAG, p.toString());
            }

            @Override
            public void onReplyCount(Post p) {
                //TODO: Start detail activity
                startPostDetailActivity(p);
            }

            @Override
            public void onShare(Post p) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "dummy text");
                setBotomSheetIntent(shareIntent);
            }
        });
    }

    private void startPostDetailActivity(Post p) {
        Intent intent = new Intent(mThisActivity, PostDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("post", p);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void setBotomSheetIntent(final Intent shareIntent) {
        mBottomSheet.showWithSheetView(new IntentPickerSheetView(mThisActivity, shareIntent, "Share with...", new IntentPickerSheetView.OnIntentPickedListener() {
            @Override
            public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                mBottomSheet.dismissSheet();
                startActivity(activityInfo.getConcreteIntent(shareIntent));
            }
        }));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(0, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    private void updateMyCircleFeed() {
        FardoSyncAdapter.syncImmediately(mThisActivity);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                FeedContract.MyCircleEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPosts.clear();
        if (data.moveToFirst()) {
            do {
                Post post = new Post();
                post.setContent(data.getString(data.getColumnIndex(FeedEntry.COLUMN_CONTENT)));
                post.setDownvotes(data.getInt(data.getColumnIndex(FeedEntry.COLUMN_DOWNVOTES)));
                post.setUpvotes(data.getInt(data.getColumnIndex(FeedEntry.COLUMN_UPVOTES)));
                post.setReplyCount(data.getInt(data.getColumnIndex(FeedEntry.COLUMN_REPLY_COUNT)));
                post.setCreatedAt(new Date());
                mPosts.add(post);
            } while (data.moveToNext());
            mAdapter.swap(mPosts);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

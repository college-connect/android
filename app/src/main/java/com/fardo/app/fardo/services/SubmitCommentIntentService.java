package com.fardo.app.fardo.services;

import android.app.IntentService;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Comment;
import com.fardo.app.fardo.models.Post;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thespidy on 22/06/16.
 */
public class SubmitCommentIntentService extends IntentService {

    private static final String TAG = "SubmitCommentService";

    public SubmitCommentIntentService() {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Comment comment = (Comment) intent.getSerializableExtra(Post.class.getSimpleName());
        String content = comment.getContent();
        //TODO: Insert the pending comments into comments table
        String url = Constants.FARDO_BASE_URL + "comments";
        JSONObject body  = new JSONObject();
        try {
            body.put("content", comment.getContent());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonRequest request = FardoApplication.getInstance().postRequest(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Boolean status = response.getBoolean("success");
                    if (!status) {
                        Log.e(TAG, "Some error occured while updating");
                        return;
                    }
                } catch (JSONException ex) {
                    Log.e(TAG, ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        }, url,body);

        FardoApplication.getInstance().getRequestQueue().add(request);
    }
}
package com.fardo.app.fardo;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.models.Post;
import com.fardo.app.fardo.services.GcmRegistrationIntentService;
import com.fardo.app.fardo.services.PrefetchIntentService;
import com.fardo.app.fardo.services.SubmitPostIntentService;
import com.fardo.app.fardo.sync.FardoSyncAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = "SplashScreen";
    public static final int PERMISSION_PHONE_STATE = 1;
    private FardoApplication appInstance;
    String mImei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appInstance = FardoApplication.getInstance();
        setContentView(R.layout.activity_splash_screen);

        String accessToken = getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE).getString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, null);
        String groupId = getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE).getString(FardoSharedPrefereces.PREFERENCE_GROUP, null);
        if (groupId != null) {
            appInstance.setGroupId(groupId);
            startPrefetchService();
        }

        if (accessToken != null) {
            startMainActivity();
            return;
        }
        checkPermissionAndRegisterUser();
    }

    private void startPrefetchService() {
        Intent intent = new Intent(this, PrefetchIntentService.class);
        startService(intent);
    }

    private void checkPermissionAndRegisterUser() {
        int accessPhoneStatePermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE);
        if (accessPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    PERMISSION_PHONE_STATE);
        } else {
            registerUser();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_PHONE_STATE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerUser();
            }
        }
    }

    private void registerUser() {
        String url = Constants.FARDO_BASE_URL + "users";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        SharedPreferences.Editor editor = getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE).edit();
        editor.putString(FardoSharedPrefereces.PREFERENCE_IMEI, telephonyManager.getDeviceId());
        editor.apply();
        try {

            JSONObject body = new JSONObject();
            body.put("imei", telephonyManager.getDeviceId());

            JsonRequest request = appInstance.postDeviceDetailsRequest(new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        afterSuccessSubmit(response.getJSONObject("data"));
                    } catch (JSONException ex) {
                        Log.e(TAG, ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                    //TODO: Handle appropriate errors like network not available
                }
            }, url, body);

            FardoApplication.getInstance().getRequestQueue().add(request);
        } catch (JSONException e) {
        }
    }

    private void afterSuccessSubmit(JSONObject response) throws JSONException {
        String token = response.getString("token");
        SharedPreferences.Editor editor = getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE).edit();
        editor.putString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, token);
        editor.apply();

        if (response.has("groupId")) {
            editor.putString(FardoSharedPrefereces.PREFERENCE_GROUP, response.getString("groupId"));
            appInstance.setGroupId(response.getString("groupId"));
            startPrefetchService();
        }
        startMainActivity();
    }

    private void startMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
    }

}

package com.fardo.app.fardo.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;

/**
 * Created by thespidy on 13/05/16.
 */
public class FeedContract {
    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "com.fardo.app.fardo";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.android.sunshine.app/weather/ is a valid path for
    // looking at weather data. content://com.example.android.sunshine.app/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.
    public static final String PATH_MY_CIRCLE = "my_circle";
    public static final String PATH_FEED = "feed_posts";
    public static final String PATH_GROUP = "group";
    public static final String PATH_LABEL = "label";
    public static final String PATH_COMMENTS = "comments";

    // To make it easy to query for the exact date, we normalize all dates that go into
    // the database to the start of the the Julian day at UTC.
    public static long normalizeDate(long startDate) {
        // normalize the start date to the beginning of the (UTC) day
        Time time = new Time();
        time.set(startDate);
        int julianDay = Time.getJulianDay(startDate, time.gmtoff);
        return time.setJulianDay(julianDay);
    }

    /* Inner class that defines the table contents of the location table */
    public static final class FeaturedGroupEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_GROUP).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_GROUP;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_GROUP;

        public static final String TABLE_NAME = "near_group";

        public static final String COLUMN_GROUP_ID = "group_id";
        public static final String COLUMN_SHORT_NAME = "group_short_name";
        public static final String COLUMN_LOCATION = "center_location";

        public static final String COLUMN_NAME = "group_name";
    }

    public static final class LabelEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LABEL).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LABEL;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LABEL;

        public static final String TABLE_NAME = "label";

        public static final String COLUMN_LABEL_ID = "label_id";
        public static final String COLUMN_GROUP_ID = "group_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";

    }

    public static final class FeedEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MY_CIRCLE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FEED;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FEED;

        public static final String TABLE_NAME = "feed_posts";

        // Date, stored as long in milliseconds since the epoch
        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_POST_ID = "post_id";

        public static final String COLUMN_CONTENT = "content";

        // Downvotes is stored as an int representing upvotes
        public static final String COLUMN_UPVOTES = "upvotes";

        // Downvotes is stored as an int representing downvotes
        public static final String COLUMN_DOWNVOTES = "downvotes";

        //Count of the number of replies came to that post
        public static final String COLUMN_LABEL_ID = "label_id";

        public static final String COLUMN_REPLY_COUNT = "reply_count";

        //If it's a pending request to resend
        public static final String COLUMN_CAN_ACT ="can_act";

        public static Uri feedByIdUri(String _id) {
            return CONTENT_URI.buildUpon().appendPath(_id).build();
        }

    }

    /* Inner class that defines the table contents of the weather table */
    public static final class MyCircleEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MY_CIRCLE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MY_CIRCLE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MY_CIRCLE;

        public static final String TABLE_NAME = "my_circle";

        // Date, stored as long in milliseconds since the epoch
        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_POST_ID = "post_id";

        public static final String COLUMN_CONTENT = "content";

        // Downvotes is stored as an int representing upvotes
        public static final String COLUMN_UPVOTES = "upvotes";

        // Downvotes is stored as an int representing downvotes
        public static final String COLUMN_DOWNVOTES = "downvotes";

        //Count of the number of replies came to that post
        public static final String COLUMN_LABEL_ID = "label_id";

        public static final String COLUMN_REPLY_COUNT = "reply_count";

        //If it's a pending request to resend
        public static final String COLUMN_PENDING_REQUEST ="pending_request";

        public static Uri myCircleByIdUri(String _id) {
            return CONTENT_URI.buildUpon().appendPath(_id).build();
        }

    }

    public static final class CommentsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COMMENTS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMMENTS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMMENTS;

        public static final String TABLE_NAME = "post_comments";

        // Date, stored as long in milliseconds since the epoch
        public static final String COLUMN_DATE = "date";

        public static final String COLUMN_COMMENT_ID = "comment_id";

        public static final String COLUMN_POST_ID = "post_id";

        public static final String COLUMN_CONTENT = "content";

        // Downvotes is stored as an int representing upvotes
        public static final String COLUMN_UPVOTES = "upvotes";

        // Downvotes is stored as an int representing downvotes
        public static final String COLUMN_DOWNVOTES = "downvotes";

        public static Uri getCommentsUriByPostId(String postId) {
            return CONTENT_URI.buildUpon().appendPath(postId).build();
        }

        public static String getPostIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }
}

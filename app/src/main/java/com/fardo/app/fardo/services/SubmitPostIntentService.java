package com.fardo.app.fardo.services;

import android.app.IntentService;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Post;

import com.fardo.app.fardo.data.FeedContract.MyCircleEntry;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thespidy on 29/05/16.
 */
public class SubmitPostIntentService extends IntentService {

    private static final String TAG = "SubmitPostService";

    public SubmitPostIntentService() {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Post post = (Post) intent.getSerializableExtra(Post.class.getSimpleName());
        String content = post.getContent();
        ContentValues categoryValues = new ContentValues();
        categoryValues.put(MyCircleEntry.COLUMN_CONTENT,content);
        //categoryValues.put(MyCircleEntry.COLUMN_LABEL_ID, labelId);
        Uri uri = getApplicationContext().getContentResolver().insert(MyCircleEntry.CONTENT_URI, categoryValues);

        submitPost(post, ContentUris.parseId(uri)); //ID of the db
    }

    private void submitPost(Post post,final long id) {
        String url = Constants.FARDO_BASE_URL + "posts";
        Gson gson = new Gson();
        JSONObject body  = new JSONObject();
        try {
            body.put("content", post.getContent());
            if(post.getLabelId()!=null && !post.getLabelId().isEmpty()) {
                body.put("labelId", post.getLabelId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonRequest request = FardoApplication.getInstance().postRequest(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Boolean status = response.getBoolean("success");
                    if (!status) {
                        return;
                    }
                    afterSuccessSubmit(response.getJSONObject("data"), id);
                } catch (JSONException ex) {
                    Log.e(TAG, ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
            }
        }, url,body);

        FardoApplication.getInstance().getRequestQueue().add(request);
    }

    private void afterSuccessSubmit(JSONObject data,long id) throws JSONException {
        String whereClause = MyCircleEntry._ID + "=" + id;
        ContentValues categoryValues = new ContentValues();
        categoryValues.put(MyCircleEntry.COLUMN_PENDING_REQUEST,0);
        categoryValues.put(MyCircleEntry.COLUMN_POST_ID,data.getString("id"));
        getApplicationContext().getContentResolver().update(MyCircleEntry.CONTENT_URI,categoryValues,whereClause,null);
    }
}

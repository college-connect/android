package com.fardo.app.fardo.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by thespidy on 14/05/16.
 */
public class FardoAuthenticatorService extends Service {

    private FardoAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new FardoAuthenticator(this);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}

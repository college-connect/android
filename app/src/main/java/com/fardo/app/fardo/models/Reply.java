package com.fardo.app.fardo.models;

/**
 * Created by thespidy on 02/05/16.
 */
public class Reply {
    private String postId;
    private String id;
    private String content;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

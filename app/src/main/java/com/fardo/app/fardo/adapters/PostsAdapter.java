package com.fardo.app.fardo.adapters;

import android.app.Activity;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fardo.app.fardo.R;
import com.fardo.app.fardo.models.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thespidy on 02/05/16.
 */
public class PostsAdapter extends RecyclerView.Adapter <PostsAdapter.PostViewHolder >{
    private final Activity mActivity;
    private List<Post> mPosts = new ArrayList<>();
    private IPostViewHolderClick mPostViewHolderClick;

    public PostsAdapter(Activity activity, List<Post> posts) {
        mPosts.clear();
        mPosts.addAll(posts);
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public Post getItem(int position) {
        return mPosts.get(position);
    }

    public void setPostViewClickListener(IPostViewHolderClick postViewClickListener) {
        mPostViewHolderClick = postViewClickListener;
    }

    class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private final TextView votes;
        ImageView upVoteBtn;
        ImageView downvoteBtn;
        TextView postContent;
        TextView replyCount;
        TextView timeAgo;
        public PostViewHolder(View itemView)
        {
            super(itemView);
            votes = (TextView) itemView.findViewById(R.id.votes);
            upVoteBtn = (ImageView) itemView.findViewById(R.id.upvoteBtn);
            downvoteBtn = (ImageView) itemView.findViewById(R.id.downvoteBtn);
            postContent = (TextView) itemView.findViewById(R.id.postContent);
            replyCount = (TextView) itemView.findViewById(R.id.replyCount);
            timeAgo = (TextView) itemView.findViewById(R.id.timeAgo);

            upVoteBtn.setOnClickListener(this);
            downvoteBtn.setOnClickListener(this);
            replyCount.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.upvoteBtn:
                    upvoteClicked(v, getAdapterPosition());
                    break;
                case R.id.downvoteBtn:
                    downvoteClicked(v,getAdapterPosition());
                    break;
                case R.id.replyCount:
                    replyCountClicked(v, getAdapterPosition());
                    break;
            }
        }

        private void replyCountClicked(View v, int position) {
            mPostViewHolderClick.onReplyCount(getItem(position));
        }

        private void downvoteClicked(View v,int position) {
            //TODO: Do some UI processing like color change to that button or something and update votescount
            mPostViewHolderClick.onDownvote(getItem(position));
        }

        private void upvoteClicked(View v,int position) {
            //TODO: Do some UI processing like color change to that button or something and update votes count
            mPostViewHolderClick.onUpvote(getItem(position));
        }

        private void shareClicked(View v,int position) {
            //TODO: Do some UI processing like color change to that button or something
            mPostViewHolderClick.onShare(getItem(position));
        }
    }

    public static interface IPostViewHolderClick {
        public void onUpvote(Post p);
        public void onDownvote(Post p);
        public void onReplyCount(Post p);
        public void onShare(Post p);
    }

    public void swap(ArrayList<Post> posts){
        mPosts.clear();
        mPosts.addAll(posts);
        notifyDataSetChanged();
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_circle_item, parent, false);

        PostViewHolder postViewHolder = new PostViewHolder(view);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Post post = mPosts.get(position);
        int votes = post.getUpvotes() - post.getDownvotes();
        holder.votes.setText(Integer.toString(votes));
        holder.postContent.setText(post.getContent());
        holder.replyCount.setText(Integer.toString(post.getReplyCount()));
        holder.timeAgo.setText("2 MIN");
    }
}



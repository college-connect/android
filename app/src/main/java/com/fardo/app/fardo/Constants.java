package com.fardo.app.fardo;

/**
 * Created by thespidy on 14/05/16.
 */
public class Constants {

    public static final String FARDO_BASE_URL_GENY_MOTION = "http://10.0.3.2:8082/api/";
    public static final String FARDO_BASE_URL_EMULATOR = "http://10.0.2.2:8082/api/";
    public static final String FARDO_PROD_URL = "http://104.199.135.175:8080/api/";
    public static final String FARDO_BASE_URL = FARDO_PROD_URL;
}

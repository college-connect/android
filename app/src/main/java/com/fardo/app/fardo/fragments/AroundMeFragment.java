package com.fardo.app.fardo.fragments;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fardo.app.fardo.R;
import com.fardo.app.fardo.adapters.PostsAdapter;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Post;
import com.flipboard.bottomsheet.BottomSheetLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class AroundMeFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LABEL_LOADER_ID = 0;
    private static final int FEATURED_LOADER_ID = 1;
    private ListView mFeaturedListView;
    private ListView mLabelListView;
    private FragmentActivity mThisActivity;
    private SimpleCursorAdapter mLabelAdapter;
    private SimpleCursorAdapter mFeaturedAdapter;

    public static AroundMeFragment newInstance(int page, String title) {
        AroundMeFragment fragment = new AroundMeFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mThisActivity = getActivity();
        View view = inflater.inflate(R.layout.fragment_around_me, container, false);
        bindViewVariables(view);
        return view;
    }

    private void bindViewVariables(View view) {
        mFeaturedListView = (ListView) view.findViewById(R.id.featuredList);
        mLabelListView = (ListView) view.findViewById(R.id.labelsList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);
        mLabelAdapter = new SimpleCursorAdapter(mThisActivity,
                R.layout.peek_label_item, null,
                new String[]{"name"},
                new int[]{R.id.labelName}, 0);
        mLabelListView.setAdapter(mLabelAdapter);
        mFeaturedAdapter = new SimpleCursorAdapter(mThisActivity,
                R.layout.peek_featured_item, null,
                new String[]{"name"},
                new int[]{R.id.groupName}, 0);
        mFeaturedListView.setAdapter(mFeaturedAdapter);
        getLoaderManager().initLoader(LABEL_LOADER_ID, null, this);
        getLoaderManager().initLoader(FEATURED_LOADER_ID, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == LABEL_LOADER_ID) {
            return new CursorLoader(mThisActivity, FeedContract.LabelEntry.CONTENT_URI, null, null, null, null);
        } else {
            return new CursorLoader(mThisActivity, FeedContract.FeaturedGroupEntry.CONTENT_URI, null, null, null, null);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == LABEL_LOADER_ID) {
            mLabelAdapter.swapCursor(data);
        } else {
            mFeaturedAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == LABEL_LOADER_ID) {
            mLabelAdapter.swapCursor(null);
        } else {
            mFeaturedAdapter.swapCursor(null);
        }
    }
}

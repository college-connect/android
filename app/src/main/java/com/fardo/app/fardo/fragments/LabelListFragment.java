package com.fardo.app.fardo.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fardo.app.fardo.R;
import com.fardo.app.fardo.adapters.DividerItemDecoration;
import com.fardo.app.fardo.adapters.LabelsAdapter;
import com.fardo.app.fardo.adapters.PostsAdapter;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Label;
import com.fardo.app.fardo.models.Post;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by thespidy on 29/05/16.
 */
public class LabelListFragment extends Fragment implements SearchView.OnQueryTextListener {

    private FragmentActivity mThisActivity;
    private RecyclerView mRecyclerView;
    private List<Label> mLabelList = new ArrayList();
    private LinearLayoutManager mLayoutManager;
    private LabelsAdapter mAdapter;

    public static LabelListFragment newInstance(int page, String title) {
        LabelListFragment fragment = new LabelListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_label_list, container, false);
        mThisActivity = getActivity();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.labelRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mThisActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        getLabelsList();
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getLabelsList() {
        Cursor data = getActivity().getContentResolver().query(FeedContract.LabelEntry.CONTENT_URI, null, null, null, null);
        assert data != null;
        if (data.moveToFirst()) {
            do {
                Label label = new Label();
                label.setId(data.getString(data.getColumnIndex(FeedContract.LabelEntry.COLUMN_LABEL_ID)));
                label.setName(data.getString(data.getColumnIndex(FeedContract.LabelEntry.COLUMN_NAME)));
                label.setDescription(data.getString(data.getColumnIndex(FeedContract.LabelEntry.COLUMN_DESCRIPTION)));
                mLabelList.add(label);
            } while (data.moveToNext());
            mAdapter = new LabelsAdapter(mThisActivity, mLabelList);
            mAdapter.setOnItemClickListener(new LabelsAdapter.LabelItemClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    returnToActivity(mAdapter.getItem(position));
                }
            });
            mRecyclerView.setAdapter(mAdapter);
        }
        data.close();
    }

    private void returnToActivity(Label label) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",label);
        mThisActivity.setResult(Activity.RESULT_OK,returnIntent);
        mThisActivity.finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        final List<Label> filteredModelList = filter(mLabelList, query);
        mAdapter.animateTo(filteredModelList);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    private List<Label> filter(List<Label> labels, String query) {
        query = query.toLowerCase();
        //TODO: Write label filter function
        final List<Label> filteredModelList = new ArrayList<>();
        for (Label label : labels) {
            final String text = label.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(label);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}

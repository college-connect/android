package com.fardo.app.fardo;

import android.app.Application;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thespidy on 14/05/16.
 */
public class FardoApplication extends Application {

    private static final String TAG = "FardoApplication";
    private static FardoApplication mInstance;
    private RequestQueue mRequestQueue;
    private String groupId;
    private double latitude;
    private double longitude;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestQueue = Volley.newRequestQueue(this);
        mInstance = this;
    }

    public static FardoApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public JsonObjectRequest postRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String url, JSONObject requestData) {
        JsonObjectRequest request = new JsonObjectRequest(url, requestData, responseListener, errorListener) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();

                String auth = "Bearer " + getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE)
                        .getString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, null);
                Log.d(TAG, auth);
                params.put("Authorization", auth);
                params.put("Origin", "android");
                return params;
            }
        };
        return request;
    }

    public JsonObjectRequest postDeviceDetailsRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String url, JSONObject requestData) {
        JsonObjectRequest request = new JsonObjectRequest(url, requestData, responseListener, errorListener) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Origin", "android");
                return params;
            }
        };
        return request;
    }

    public JsonObjectRequest putRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String url, JSONObject requestData) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, requestData, responseListener, errorListener) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String auth = "Bearer " + getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE)
                        .getString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, null);
                params.put("Authorization", auth);
                params.put("Origin", "android");
                return params;
            }
        };
        return request;
    }

    public JsonObjectRequest getRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String url) {
        JsonObjectRequest request = new JsonObjectRequest(url, null, responseListener, errorListener) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String auth = "Bearer " + getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE)
                        .getString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, null);
                params.put("Authorization", auth);
                params.put("Origin", "android");
                return params;
            }
        };
        return request;
    }

    public JsonObjectRequest locationBasedGetRequest(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String url) {
        JsonObjectRequest request = new JsonObjectRequest(url, null, responseListener, errorListener) {
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                String auth = "Bearer " + getSharedPreferences(FardoSharedPrefereces.FARDO_PREFS, MODE_PRIVATE)
                        .getString(FardoSharedPrefereces.PREFERENCE_ACCESSTOKEN, null);
                params.put("Authorization", auth);
                params.put("Origin", "android");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                        params.put("lat", Double.toString(getLatitude()));
                        params.put("lng", Double.toString(getLongitude()));

                return params;
            }
        };
        return request;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

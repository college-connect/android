package com.fardo.app.fardo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.fardo.app.fardo.fragments.PostDetailFragment;
import com.fardo.app.fardo.models.Post;

/**
 * Created by thespidy on 24/06/16.
 */
public class FeedActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String titleName= bundle.getString("name");
        ab.setTitle(titleName);
        Bundle args = new Bundle();
        Fragment fragment = PostDetailFragment.newInstance(1,"post details");
        fragment.setArguments(args);
        ft.add(R.id.postDetails,fragment);
        ft.commit();
    }
}

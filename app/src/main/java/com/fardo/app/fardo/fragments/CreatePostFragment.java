package com.fardo.app.fardo.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.LabelSelectActivity;
import com.fardo.app.fardo.MainActivity;
import com.fardo.app.fardo.R;
import com.fardo.app.fardo.models.Label;
import com.fardo.app.fardo.models.Post;
import com.fardo.app.fardo.services.SubmitPostIntentService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thespidy on 26/05/16.
 */
public class CreatePostFragment extends Fragment {

    private static final int LABEL_SELECT_REQUEST = 100;
    private final String TAG = "CreatePostFragment";
    private EditText mContentTxt;
    private TextView mTagTxt;
    private Post mPost;
    private MenuItem mCreatePostMenuItem;
    private FragmentActivity mthisActivity;
    private Button mTagBtn;
    private Label mAttachedLabel;


    public static CreatePostFragment newInstance(int page, String title) {
        CreatePostFragment fragment = new CreatePostFragment();
        Bundle args = new Bundle();
        args.putInt("dummy", page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        mthisActivity = getActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_post, container, false);
        bindViewVariables(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mCreatePostMenuItem = menu.findItem(R.id.action_create_post);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void bindViewVariables(View view) {
        mContentTxt = (EditText) view.findViewById(R.id.content);
        mTagTxt = (TextView) view.findViewById(R.id.tagTxt);
        mTagBtn = (Button) view.findViewById(R.id.tagBtn);
        mTagBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLabelSelectActivity();
            }
        });
        mContentTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (mCreatePostMenuItem == null) {
                    return;
                }
                if (!s.toString().isEmpty()) {
                    mCreatePostMenuItem.setEnabled(true);
                } else {
                    mCreatePostMenuItem.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });
    }

    private void startLabelSelectActivity() {
        Intent intent = new Intent(mthisActivity, LabelSelectActivity.class);
        startActivityForResult(intent,LABEL_SELECT_REQUEST);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.action_create_post:
                submitPost();
                break;
        }
        return true;
    }

    private void submitPost() {
        if (!isValidPost()) {
            return;
        }

        mPost = new Post();
        mPost.setContent(mContentTxt.getText().toString());
        if(mAttachedLabel!=null) {
           mPost.setLabelId( mAttachedLabel.getId());
        }
        Intent intent = new Intent(mthisActivity, SubmitPostIntentService.class);
        intent.putExtra(Post.class.getSimpleName(), mPost);
        mthisActivity.startService(intent);
        Intent activityIntent = new Intent(mthisActivity, MainActivity.class);
        startActivity(activityIntent);
    }

    private boolean isValidPost() {
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == LABEL_SELECT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                Label label = (Label)data.getSerializableExtra("result");
                setLabel(label);
            }
        }
    }

    private void setLabel(Label label) {
        mAttachedLabel = label;
        mTagTxt.setText(label.getName());
    }
}

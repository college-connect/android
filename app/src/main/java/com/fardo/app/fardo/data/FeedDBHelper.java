package com.fardo.app.fardo.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fardo.app.fardo.data.FeedContract.MyCircleEntry;
import com.fardo.app.fardo.data.FeedContract.FeaturedGroupEntry;
import com.fardo.app.fardo.data.FeedContract.LabelEntry;
import com.fardo.app.fardo.data.FeedContract.CommentsEntry;

/**
 * Created by thespidy on 13/05/16.
 */
public class FeedDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 5;

    static final String DATABASE_NAME = "feed.db";

    public FeedDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_MY_CIRCLE_TABLE = "CREATE TABLE " + MyCircleEntry.TABLE_NAME + " (" +
                MyCircleEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                MyCircleEntry.COLUMN_POST_ID + " TEXT," +
                MyCircleEntry.COLUMN_CONTENT + " TEXT NOT NULL," +
                MyCircleEntry.COLUMN_DATE + " INTEGER," +
                MyCircleEntry.COLUMN_DOWNVOTES + " INTEGER DEFAULT 0," +
                MyCircleEntry.COLUMN_UPVOTES + " INTEGER DEFAULT 0," +
                MyCircleEntry.COLUMN_LABEL_ID + " INTEGER," +
                MyCircleEntry.COLUMN_REPLY_COUNT + " INTEGER, " +
                MyCircleEntry.COLUMN_PENDING_REQUEST + " INTEGER DEFAULT 0 " +
                " );";

        Log.d("SQLITE", SQL_CREATE_MY_CIRCLE_TABLE);

        final String SQL_CREATE_GROUP_TABLE = "CREATE TABLE " + FeaturedGroupEntry.TABLE_NAME + " (" +
                FeaturedGroupEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                FeaturedGroupEntry.COLUMN_GROUP_ID + " TEXT NOT NULL," +
                FeaturedGroupEntry.COLUMN_NAME + " TEXT NOT NULL," +
                FeaturedGroupEntry.COLUMN_SHORT_NAME + " TEXT " +
                " );";

        Log.d("SQLITE", SQL_CREATE_GROUP_TABLE);

        final String SQL_CREATE_LABEL_TABLE = "CREATE TABLE " + LabelEntry.TABLE_NAME + " (" +
                LabelEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                LabelEntry.COLUMN_LABEL_ID + " TEXT NOT NULL," +
                LabelEntry.COLUMN_GROUP_ID + " TEXT," +
                LabelEntry.COLUMN_NAME + " TEXT NOT NULL," +
                LabelEntry.COLUMN_DESCRIPTION + " TEXT " +
                " );";

        final String SQL_CREATE_COMMENTS_TABLE = "CREATE TABLE " + CommentsEntry.TABLE_NAME + " (" +
                CommentsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                CommentsEntry.COLUMN_COMMENT_ID + " TEXT," +
                CommentsEntry.COLUMN_POST_ID + " TEXT," +
                CommentsEntry.COLUMN_CONTENT + " TEXT NOT NULL," +
                CommentsEntry.COLUMN_DATE + " INTEGER," +
                CommentsEntry.COLUMN_DOWNVOTES + " INTEGER DEFAULT 0," +
                CommentsEntry.COLUMN_UPVOTES + " INTEGER DEFAULT 0" +
                " );";

        Log.d("SQLITE", SQL_CREATE_MY_CIRCLE_TABLE);

        sqLiteDatabase.execSQL(SQL_CREATE_MY_CIRCLE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_GROUP_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_LABEL_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_COMMENTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MyCircleEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FeaturedGroupEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LabelEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CommentsEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}


package com.fardo.app.fardo.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.R;
import com.fardo.app.fardo.adapters.CommentsAdapter;
import com.fardo.app.fardo.adapters.DividerItemDecoration;
import com.fardo.app.fardo.adapters.PostsAdapter;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Comment;
import com.fardo.app.fardo.models.Post;


import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by thespidy on 28/05/16.
 */
public class PostDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {
    private static final String TAG = "postDetailFragment";
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private CommentsAdapter mAdapter;
    private Activity mThisActivity;
    private ArrayList<Comment> mComments;
    private Post mPost;
    private ImageView upvoteBtn;
    private ImageView downvoteBtn;
    private ImageView postCommentBtn;
    private TextView shareBtn;
    private TextView commentContentText;

    public static PostDetailFragment newInstance(int page, String title) {
        PostDetailFragment fragment = new PostDetailFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_detail, container, false);
        Bundle args = getArguments();
        mPost = (Post) args
                .getSerializable("post");
        bindViewVariables(view);
        return view;
    }

    private void bindViewVariables(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_circle_recycler);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        upvoteBtn = (ImageView)view.findViewById(R.id.upvoteBtn);
        downvoteBtn = (ImageView)view.findViewById(R.id.downvoteBtn);
        postCommentBtn = (ImageView)view.findViewById(R.id.postComment);
        commentContentText = (TextView)view.findViewById(R.id.commentContent);
        shareBtn = (TextView)view.findViewById(R.id.share);
        mLayoutManager = new LinearLayoutManager(mThisActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mAdapter = new CommentsAdapter(mThisActivity, mComments);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        upvoteBtn.setOnClickListener(this);
        downvoteBtn.setOnClickListener(this);
        postCommentBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);

        mAdapter.setCommentViewClickListener(new CommentsAdapter.ICommentViewHolderClick() {
            @Override
            public void onUpvote(Comment p) {
                submitCommentUpvote(p.getId());
            }

            @Override
            public void onDownvote(Comment p) {
                submitCommentDownvote(p.getId());
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.upvoteBtn:
                submitUpvote();
                break;
            case R.id.downvoteBtn:
                submitDownvote();
                break;
            case R.id.share:
                break;
            case R.id.postComment:
                submitComment();
                break;
        }
    }

    private void submitComment() {
        String  content = commentContentText.getText().toString();
        if(content.isEmpty()) {
            invalidComment("Comment cannot be empty");
            return;
        }
        insertComment();
        //TODO: Insert the pending comments into comments table
        String url = Constants.FARDO_BASE_URL + "comments";
        JSONObject body  = new JSONObject();
        try {
            body.put("content", content);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonRequest request = FardoApplication.getInstance().postRequest(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Boolean status = response.getBoolean("success");
                    if (!status) {
                        Log.e(TAG, "Some error occured while updating");
                        return;
                    }
                } catch (JSONException ex) {
                    Log.e(TAG, ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        }, url,body);

        FardoApplication.getInstance().getRequestQueue().add(request);
    }

    private void insertComment() {
        //TODO: Insert comment into comment list
    }

    private void invalidComment(String message) {
        Toast toast = Toast.makeText(mThisActivity, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void submitDownvote() {
        submitVote(false);
    }

    private void submitUpvote() {
        submitVote(true);
    }

    private void submitVote(boolean isUpvote) {
        try {
            FardoApplication appInstance = FardoApplication.getInstance();
            String url = Constants.FARDO_BASE_URL + "posts/" + mPost.getId();
            if(isUpvote) {
                url += "/upvote";
            }
            else{
                url += "/downvote";
            }

            JsonRequest request = appInstance.putRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Vote successfully updated");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                }
            }, url, null);

            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }

    private void submitCommentUpvote(String commentId) {
        submitCommentVote(commentId, true);
    }

    private void submitCommentDownvote(String commentId) {
        submitCommentVote(commentId, false);
    }

    private void submitCommentVote(String commentId, boolean isUpvote) {
        try {
            FardoApplication appInstance = FardoApplication.getInstance();
            String url = Constants.FARDO_BASE_URL + "comments/" + commentId;
            if (isUpvote) {
                url += "/upvote";
            } else {
                url += "/downvote";
            }

            JsonRequest request = appInstance.putRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Comment Vote successfully updated");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                }
            }, url, null);

            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                FeedContract.CommentsEntry.getCommentsUriByPostId(mPost.getId()),
                null,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mComments.clear();
        if (data.moveToFirst()) {
            do {
                Comment comment = new Comment();
                comment.setContent(data.getString(data.getColumnIndex(FeedContract.CommentsEntry.COLUMN_CONTENT)));
                comment.setDownvotes(data.getInt(data.getColumnIndex(FeedContract.CommentsEntry.COLUMN_DOWNVOTES)));
                comment.setUpvotes(data.getInt(data.getColumnIndex(FeedContract.CommentsEntry.COLUMN_UPVOTES)));
                comment.setCreatedAt(new Date());
                mComments.add(comment);
            } while (data.moveToNext());
            mAdapter.swap(mComments);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
package com.fardo.app.fardo.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.fardo.app.fardo.data.FeedContract.FeaturedGroupEntry;
import com.fardo.app.fardo.data.FeedContract.MyCircleEntry;
import com.fardo.app.fardo.data.FeedContract.LabelEntry;
import com.fardo.app.fardo.data.FeedContract.CommentsEntry;

/**
 * Created by thespidy on 13/05/16.
 */
public class FeedProvider extends ContentProvider{
    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private FeedDBHelper mOpenHelper;

    static final int MY_CIRCLE_FEED = 100;
    static final int GROUPS = 200;
    static final int LABELS = 300;
    static final int COMMENTS = 400;
    static final int POST_COMMENTS = 401;


    private static final String sCommentsSelectionByPostID =
            FeedContract.CommentsEntry.TABLE_NAME+
                    "." + FeedContract.CommentsEntry.COLUMN_POST_ID + " = ? ";

    private Cursor getMyCircleFeed(Uri uri) {
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return db.query(MyCircleEntry.TABLE_NAME,null,null,null,null,null,null);
    }

    private Cursor getLabels(Uri uri) {
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return db.query(LabelEntry.TABLE_NAME,null,null,null,null,null,null);
    }

    private Cursor getPostComments(Uri uri, String[] projection, String sortOrder) {
        String postId = FeedContract.CommentsEntry.getPostIdFromUri(uri);
        String selection = sCommentsSelectionByPostID;
        String[] selectionArgs = new String[]{postId};
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return db.query(FeedContract.CommentsEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

    }


    static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = FeedContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, FeedContract.PATH_MY_CIRCLE,MY_CIRCLE_FEED);
        matcher.addURI(authority, FeedContract.PATH_GROUP,GROUPS);
        matcher.addURI(authority, FeedContract.PATH_LABEL,LABELS);
        matcher.addURI(authority, FeedContract.PATH_COMMENTS + "/*", POST_COMMENTS);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new FeedDBHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {

        final int match = sUriMatcher.match(uri);

        switch (match) {
            case MY_CIRCLE_FEED:
                return MyCircleEntry.CONTENT_TYPE;
            case GROUPS:
                return FeaturedGroupEntry.CONTENT_TYPE;
            case LABELS:
                return FeaturedGroupEntry.CONTENT_TYPE;
            case POST_COMMENTS:
                return CommentsEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            // "weather/*/*"
            case MY_CIRCLE_FEED:
                retCursor = getMyCircleFeed(uri);
                break;
            case LABELS:
                retCursor = getLabels(uri);
                break;
            case POST_COMMENTS:
                retCursor = getPostComments(uri, projection, sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    /*
        Student: Add the ability to insert Locations to the implementation of this function.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MY_CIRCLE_FEED: {
                long _id = db.insert(MyCircleEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = MyCircleEntry.myCircleByIdUri(Long.toString(_id));
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if (null == selection) selection = "1";
        switch (match) {
            case MY_CIRCLE_FEED:
                rowsDeleted = db.delete(
                        MyCircleEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case LABELS:
                rowsDeleted = db.delete(
                        LabelEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case COMMENTS:
                rowsDeleted = db.delete(
                        CommentsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case MY_CIRCLE_FEED:
                rowsUpdated = db.update(MyCircleEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case LABELS:
                rowsUpdated = db.update(MyCircleEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case COMMENTS:
                rowsUpdated = db.update(CommentsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case MY_CIRCLE_FEED:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(MyCircleEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case LABELS:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(FeedContract.LabelEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            case COMMENTS:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(CommentsEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}

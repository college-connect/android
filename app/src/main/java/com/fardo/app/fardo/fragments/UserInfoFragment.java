package com.fardo.app.fardo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fardo.app.fardo.R;

/**
 * Created by thespidy on 28/05/16.
 */
public class UserInfoFragment extends Fragment {

    public static UserInfoFragment newInstance(int page, String title) {
        UserInfoFragment fragment = new UserInfoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }
}

package com.fardo.app.fardo.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by thespidy on 17/05/16.
 */
public class FardoSyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();
    private static FardoSyncAdapter sFardoSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sFardoSyncAdapter == null) {
                sFardoSyncAdapter = new FardoSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sFardoSyncAdapter.getSyncAdapterBinder();
    }
}

package com.fardo.app.fardo.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.PostDetailActivity;
import com.fardo.app.fardo.R;
import com.fardo.app.fardo.adapters.DividerItemDecoration;
import com.fardo.app.fardo.adapters.PostsAdapter;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Post;
import com.fardo.app.fardo.sync.FardoSyncAdapter;
import com.fardo.app.fardo.data.FeedContract.MyCircleEntry;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCircleFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "MyCircleFragment";
    private Activity mThisActivity;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private PostsAdapter mAdapter;
    private ArrayList<Post> mPosts = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Handler handler = new Handler();
    private BottomSheetLayout mBottomSheet;

    // newInstance constructor for creating fragment with arguments
    public static MyCircleFragment newInstance(int page, String title) {
        MyCircleFragment fragmentFirst = new MyCircleFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThisActivity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_circle, container, false);
        bindViewVariables(view);
        return view;
    }

    private void bindViewVariables(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_circle_recycler);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mThisActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PostsAdapter(mThisActivity, mPosts);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_my_circle);
        mBottomSheet = (BottomSheetLayout) view.findViewById(R.id.bottomsheet);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG, "Swipe refresh");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mAdapter.setPostViewClickListener(new PostsAdapter.IPostViewHolderClick() {
            @Override
            public void onUpvote(Post p) {
                submitUpvote(p.getId());
            }

            @Override
            public void onDownvote(Post p) {
                submitDownvote(p.getId());
            }

            @Override
            public void onReplyCount(Post p) {
                //TODO: Start detail activity
                startPostDetailActivity(p);
            }

            @Override
            public void onShare(Post p) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "dummy text");
                setBotomSheetIntent(shareIntent);
            }
        });
    }

    private void submitVote(String postId,boolean isUpvote) {
        try {
            FardoApplication appInstance = FardoApplication.getInstance();
            String url = Constants.FARDO_BASE_URL + "posts/" + postId;
            if(isUpvote) {
                url += "/upvote";
            }
            else{
                url += "/downvote";
            }

            JsonRequest request = appInstance.putRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Vote successfully updated");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                }
            }, url, null);

            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }

    private void submitUpvote(String postId) {
        submitVote(postId, true);
    }

    private void submitDownvote(String postId) {
        submitVote(postId, false);
    }

    private void startPostDetailActivity(Post p) {
        Intent intent = new Intent(mThisActivity, PostDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("post", p);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void setBotomSheetIntent(final Intent shareIntent) {
        mBottomSheet.showWithSheetView(new IntentPickerSheetView(mThisActivity, shareIntent, "Share with...", new IntentPickerSheetView.OnIntentPickedListener() {
            @Override
            public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                mBottomSheet.dismissSheet();
                startActivity(activityInfo.getConcreteIntent(shareIntent));
            }
        }));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(0, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    private void updateMyCircleFeed() {
        FardoSyncAdapter.syncImmediately(mThisActivity);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                FeedContract.MyCircleEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPosts.clear();
        if (data.moveToFirst()){
            do{
                Post post = new Post();
                post.setContent(data.getString(data.getColumnIndex(MyCircleEntry.COLUMN_CONTENT)));
                post.setDownvotes(data.getInt(data.getColumnIndex(MyCircleEntry.COLUMN_DOWNVOTES)));
                post.setUpvotes(data.getInt(data.getColumnIndex(MyCircleEntry.COLUMN_UPVOTES)));
                post.setReplyCount(data.getInt(data.getColumnIndex(MyCircleEntry.COLUMN_REPLY_COUNT)));
                post.setReplyCount(data.getInt(data.getColumnIndex(MyCircleEntry.COLUMN_PENDING_REQUEST)));
                post.setCreatedAt(new Date());
                mPosts.add(post);
            }while(data.moveToNext());
            mAdapter.swap(mPosts);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
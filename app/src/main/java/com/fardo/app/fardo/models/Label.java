package com.fardo.app.fardo.models;

import android.graphics.Point;

import java.io.Serializable;

/**
 * Created by thespidy on 29/05/16.
 */
public class Label implements Serializable {
    private String id;
    private String name;
    private String description;
    private boolean isVerified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.fardo.app.fardo;

/**
 * Created by thespidy on 14/05/16.
 */
public class FardoSharedPrefereces {
    public static final String FARDO_PREFS = "fardo_prefs";
    public static final String PREFERENCE_ACCESSTOKEN = "pref_accesstoken";
    public static final String PREFERENCE_LATITUDE  = "pref_latitude";
    public static final String PREFERENCE_LONGITUDE  = "pref_longitude";
    public static final String PREFERENCE_GROUP  = "pref_group";
    public static final String PREFERENCE_IMEI  = "imei_number";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";


}

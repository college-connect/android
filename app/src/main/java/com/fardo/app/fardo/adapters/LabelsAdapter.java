package com.fardo.app.fardo.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fardo.app.fardo.R;
import com.fardo.app.fardo.models.Label;
import com.fardo.app.fardo.models.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thespidy on 30/05/16.
 */
public class LabelsAdapter extends RecyclerView.Adapter <LabelsAdapter.LabelViewHolder >{
    private final Activity mActivity;
    private List<Label> mLabels = new ArrayList<>();
    private static LabelItemClickListener mLabelItemClickListener;

    public LabelsAdapter(Activity activity, List<Label> labels) {
        mLabels.addAll(labels);
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mLabels.size();
    }

    public Label getItem(int position) {
        return mLabels.get(position);
    }

    public void addItem(int position, Label label) {
        mLabels.add(position, label);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Label model = mLabels.remove(fromPosition);
        mLabels.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    private void removeItem(int i) {
        mLabels.remove(i);
    }

    public void animateTo(List<Label> labels) {
        applyAndAnimateRemovals(labels);
        applyAndAnimateAdditions(labels);
        applyAndAnimateMovedItems(labels);
    }

    private void applyAndAnimateRemovals(List<Label> newLabels) {
        for (int i = mLabels.size() - 1; i >= 0; i--) {
            final Label model = mLabels.get(i);
            if (!newLabels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Label> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Label model = newModels.get(i);
            if (!mLabels.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Label> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Label model = newModels.get(toPosition);
            final int fromPosition = mLabels.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    class LabelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView labelName;
        TextView labelDescription;
        public LabelViewHolder(View itemView)
        {
            super(itemView);
            labelName = (TextView) itemView.findViewById(R.id.labelName);
            labelDescription = (TextView) itemView.findViewById(R.id.labelDescription);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mLabelItemClickListener.onItemClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListener(LabelItemClickListener labelClickListener) {
        this.mLabelItemClickListener = labelClickListener;
    }

    @Override
    public LabelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.label_item, parent, false);

        LabelViewHolder postViewHolder = new LabelViewHolder(view);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(LabelViewHolder holder, int position) {
        Label label = mLabels.get(position);
        holder.labelName.setText(label.getName());
        holder.labelDescription.setText(label.getDescription());
    }

    public interface LabelItemClickListener {
        public void onItemClick(int position, View v);
    }
}

package com.fardo.app.fardo.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.fardo.app.fardo.Constants;
import com.fardo.app.fardo.FardoApplication;
import com.fardo.app.fardo.MainActivity;
import com.fardo.app.fardo.R;
import com.fardo.app.fardo.data.FeedContract;
import com.fardo.app.fardo.models.Post;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class FardoSyncAdapter extends AbstractThreadedSyncAdapter {
    public final String LOG_TAG = FardoSyncAdapter.class.getSimpleName();
    public static final int SYNC_INTERVAL = 60 * 180;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;


    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;
    private static final int MY_CIRCLE_NOTIFICATION_ID = 3001;

    /**
     * Set up the sync adapter
     */
    public FardoSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(LOG_TAG, "Starting sync");
        performMyCircleSync();
        performFeaturedGroupSync();
    }

    private void performFeaturedGroupSync() {
        try {

            final String url = Constants.FARDO_BASE_URL + "featured-groups";

            FardoApplication appInstance = FardoApplication.getInstance();

            JsonRequest request = appInstance.locationBasedGetRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        getFeaturedGroupsDataFromJson(response.getJSONArray("data"));
                    } catch (JSONException ex) {

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOG_TAG, error.toString());
                }
            }, url);
            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(LOG_TAG, ex.toString());
        }
    }

    private void getFeaturedGroupsDataFromJson(JSONArray groupsArray) throws JSONException {
        Vector<ContentValues> postsContentVector = new Vector<ContentValues>(groupsArray.length());
        for (int i = 0; i < groupsArray.length(); i++) {

            ContentValues groupValues = new ContentValues();
            JSONObject groups = groupsArray.getJSONObject(i);

            groupValues.put(FeedContract.FeaturedGroupEntry.COLUMN_GROUP_ID, groups.getString("id"));
            groupValues.put(FeedContract.FeaturedGroupEntry.COLUMN_NAME, groups.getString("name"));
            groupValues.put(FeedContract.FeaturedGroupEntry.COLUMN_SHORT_NAME, groups.getString("shortName"));
            postsContentVector.add(groupValues);
        }

        if (postsContentVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[postsContentVector.size()];
            postsContentVector.toArray(cvArray);

            getContext().getContentResolver().delete(FeedContract.FeaturedGroupEntry.CONTENT_URI, null, null);
            getContext().getContentResolver().bulkInsert(FeedContract.FeaturedGroupEntry.CONTENT_URI, cvArray);
            notifyMyCircle();
        }

        Log.d(LOG_TAG, "Sync Complete. " + postsContentVector.size() + " Inserted");

    }

    private void performMyCircleSync() {
        try {

            final String url = Constants.FARDO_BASE_URL + "my-circle";

            FardoApplication appInstance = FardoApplication.getInstance();

            JsonRequest request = appInstance.locationBasedGetRequest(new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        geMyCircleDataFromJson(response.getJSONArray("data"));
                    } catch (JSONException ex) {

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOG_TAG, error.toString());
                }
            }, url);
            appInstance.getRequestQueue().add(request);
        } catch (Exception ex) {
            Log.e(LOG_TAG, ex.toString());
        }
    }

    private void geMyCircleDataFromJson(JSONArray myCircleArray)
            throws JSONException {

        Vector<ContentValues> postsContentVector = new Vector<ContentValues>(myCircleArray.length());
        for (int i = 0; i < myCircleArray.length(); i++) {

            ContentValues myCircleValues = new ContentValues();
            JSONObject posts = myCircleArray.getJSONObject(i);

            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_POST_ID, "123ABC");
            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_DATE, 111111);
            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_UPVOTES, posts.getInt("upvotes"));
            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_DOWNVOTES, posts.getInt("downvotes"));
            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_CONTENT, posts.getString("content"));
            myCircleValues.put(FeedContract.MyCircleEntry.COLUMN_REPLY_COUNT, posts.getInt("replyCount"));

            postsContentVector.add(myCircleValues);
        }

        if (postsContentVector.size() > 0) {
            ContentValues[] cvArray = new ContentValues[postsContentVector.size()];
            postsContentVector.toArray(cvArray);

            getContext().getContentResolver().delete(FeedContract.MyCircleEntry.CONTENT_URI, null, null);
            getContext().getContentResolver().bulkInsert(FeedContract.MyCircleEntry.CONTENT_URI, cvArray);
        }

        Log.d(LOG_TAG, "Sync Complete. " + postsContentVector.size() + " Inserted");

    }

    private void notifyMyCircle() {

        //TODO: We will query our contentProvider to get data and notify using NotificationManager by opening a cursor, research about TaskStackBuilder

    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        if (ContentResolver.isSyncPending(getSyncAccount(context),
                context.getString(R.string.content_authority)) ||
                ContentResolver.isSyncActive(getSyncAccount(context),
                        context.getString(R.string.content_authority))) {
            Log.d("ContentResolver", "SyncPending, canceling");
            ContentResolver.cancelSync(getSyncAccount(context),
                    context.getString(R.string.content_authority));
        }

        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }

    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Finally, let's do a sync to get things started
         */
        FardoSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }


}

package com.fardo.app.fardo.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fardo.app.fardo.R;
import com.fardo.app.fardo.models.Comment;
import com.fardo.app.fardo.models.Post;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter <CommentsAdapter.CommentViewHolder >{
    private final Activity mActivity;
    private List<Comment> mComments = new ArrayList<>();
    private ICommentViewHolderClick mPostViewHolderClick;

    public CommentsAdapter(Activity activity, List<Comment> comments) {
        mComments.clear();
        mComments.addAll(comments);
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public Comment getItem(int position) {
        return mComments.get(position);
    }

    public void setCommentViewClickListener(ICommentViewHolderClick postViewClickListener) {
        mPostViewHolderClick = postViewClickListener;
    }

    class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private final TextView votes;
        ImageView upVoteBtn;
        ImageView downvoteBtn;
        TextView commentContent;
        TextView timeAgo;
        public CommentViewHolder(View itemView)
        {
            super(itemView);
            votes = (TextView) itemView.findViewById(R.id.votes);
            upVoteBtn = (ImageView) itemView.findViewById(R.id.upvoteBtn);
            downvoteBtn = (ImageView) itemView.findViewById(R.id.downvoteBtn);
            commentContent = (TextView) itemView.findViewById(R.id.commentContent);
            timeAgo = (TextView) itemView.findViewById(R.id.timeAgo);

            upVoteBtn.setOnClickListener(this);
            downvoteBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.upvoteBtn:
                    upvoteClicked(v, getAdapterPosition());
                    break;
                case R.id.downvoteBtn:
                    downvoteClicked(v,getAdapterPosition());
                    break;
            }
        }

        private void downvoteClicked(View v,int position) {
            //TODO: Do some UI processing like color change to that button or something
            mPostViewHolderClick.onDownvote(getItem(position));
        }

        private void upvoteClicked(View v,int position) {
            //TODO: Do some UI processing like color change to that button or something
            mPostViewHolderClick.onUpvote(getItem(position));
        }
    }

    public static interface ICommentViewHolderClick {
        public void onUpvote(Comment p);
        public void onDownvote(Comment p);
    }

    public void swap(ArrayList<Comment> comments){
        mComments.clear();
        mComments.addAll(comments);
        notifyDataSetChanged();
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = mComments.get(position);
        int votes = comment.getUpvotes() - comment.getDownvotes();
        holder.votes.setText(Integer.toString(votes));
        holder.commentContent.setText(comment.getContent());
        holder.timeAgo.setText("2 MIN");
    }
}
